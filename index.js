const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const fs = require('fs');
const readline = require('readline');

mongoose.connect('mongodb://localhost:27017/MongoDbPractice', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to mongodb');

        // Read file to get each JSON
        const fileStream = fs.createReadStream('./restaurants.json');
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity,
        });

        const restaurantSchema = new Schema({}, { strict: false });
        const Restaurant = mongoose.model('Restaurant', restaurantSchema);

        rl.on('line', function (line) {
            // Insert into mongodb
            const restaurantObj = new Restaurant(JSON.parse(line));
            restaurantObj.save().then(() => {
                console.log('Record inserted');
            }).catch(err => {
                console.log(err);
            });
        })

    }).catch((err) => {
        console.log(err);
    });
